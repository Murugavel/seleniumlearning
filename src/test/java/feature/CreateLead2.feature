Feature: Creating the Lead

Scenario Outline:: Create Lead 1
And Enter the Username as <username>
And Enter the Password as <password>
And Click submit button
And Click CRMSFA
And Click Leads
And Click Create Lead
And Enter Company Name as <cName>
And Enter First Name as <fName>
And Enter Last Name as <lName>
When Click Create Lead Submit button
Then Verify First Name as <FirstName>
Examples:
|username|password|cName|fName|lName|FirstName|
|DemoSalesManager|crmsfa|BNY|Senthil|Prabu|Senthil|
|DemoSalesManager|crmsfa|Aon|Ganesh|Babu|Ganesh|


