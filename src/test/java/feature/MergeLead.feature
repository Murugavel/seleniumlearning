Feature: Merging the Lead

Scenario Outline:: Merge Lead
And Enter the Username as <username>
And Enter the Password as <password>
And Click submit button
And Click CRMSFA
And Click Leads
And Click Merge Lead
And Click From Lead
And Enter Lead ID For Merge as <FromLeadID>
And Click Find Lead Button For Merge
And Click First Lead ID For Merge
And Click To Lead
And Enter Lead ID For Merge as <ToLeadID>
And Click Find Lead Button For Merge
And Click First Lead ID For Merge
And Click Merge Lead Button
And Click Find Leads From View
And Enter Lead ID in Find Lead as <fLeadID>
When Click Find Leads Button
Then Verify Lead ID in Find Leads
Examples:
|username|password|FromLeadID|ToLeadID|fLeadID|
|DemoSalesManager|crmsfa|10051|10052|10051|
|DemoSalesManager|crmsfa|10062|10063|10062|


