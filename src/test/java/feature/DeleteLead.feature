Feature: Deleting the Lead

Scenario Outline:: Delete Lead
And Enter the Username as <username>
And Enter the Password as <password>
And Click submit button
And Click CRMSFA
And Click Leads
And Click Find Lead
And Enter Lead ID in Find Lead as <lID>
And Click Find Leads Button
And Click First Leads ID
And Click Delete Button
And Click Find Lead
And Enter Lead ID in Find Lead as <leadID>
When Click Find Leads Button
Then Verify Lead ID in Find Leads
Examples:
|username|password|lID|leadID|
|DemoSalesManager|crmsfa|10038|10038|
|DemoSalesManager|crmsfa|10039|10039|


