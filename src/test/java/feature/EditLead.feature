Feature: Editing the Lead

Scenario Outline:: Edit Lead
And Enter the Username as <username>
And Enter the Password as <password>
And Click submit button
And Click CRMSFA
And Click Leads
And Click Find Lead
And Enter First Name in Find Lead as <fName>
And Click Find Leads Button
And Click First Leads ID
And Click Edit Button
And Change Company Name as <cName>
When Click Update button
Then Edit Lead is Success
Examples:
|username|password|fName|cName|
|DemoSalesManager|crmsfa|Muru|Amazon|
|DemoSalesManager|crmsfa|Ganesh|Zoho|


