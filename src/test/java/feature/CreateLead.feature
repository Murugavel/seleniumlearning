Feature: Creating the Lead
Background:
Given Open the browser
And Maximize the browser
And Load the URL

@reg
Scenario: Create Lead
And Enter the Username as DemoSalesManager
And Enter the Password as crmsfa
And Click submit button
And Click CRM/SFA
And Click Leads
And Click Create Lead
And Enter Company Name as Fid
And Enter First Name as Muru
And Enter Last Name as Vel
And Click Create Lead Submit button
When Verify Company Name as Fid
Then Verify Create Lead is success

@smoke @reg
Scenario Outline:: Create Lead 1
And Enter the Username as <username>
And Enter the Password as <password>
And Click submit button
And Click CRM/SFA
And Click Leads
And Click Create Lead
And Enter Company Name as <cName>
And Enter First Name as <fName>
And Enter Last Name as <lName>
And Click Create Lead Submit button
When Verify Company Name as <ComName>
Then Verify Create Lead is success
Examples:
|username|password|cName|fName|lName|ComName|
|DemoSalesManager|crmsfa|BNY|Senthil|Prabu|BNY|
|DemoSalesManager|crmsfa|Aon|Ganesh|Babu|Aon|

