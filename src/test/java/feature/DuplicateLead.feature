Feature: Duplicate the Lead

Scenario Outline:: Duplicate Lead
And Enter the Username as <username>
And Enter the Password as <password>
And Click submit button
And Click CRMSFA
And Click Leads
And Click Find Lead
And Enter Company Name in Find Lead as <cName>
And Click Find Leads Button
And Click First Leads ID
And Click Duplicate Lead Button
When Click Duplicate Submit
Then Verify First Name as <fName>
Examples:
|username|password|cName|fName|
|DemoSalesManager|crmsfa|Fid|Muru|
|DemoSalesManager|crmsfa|TCS|Leela|


