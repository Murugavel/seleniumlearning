/*package Steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver;
	
	@Given("Open the browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	}

	@And("Maximize the browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	    	}

	@And("Load the URL")
	public void loadTheURL() {
	    driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@And("Enter the Username as (.*)")
	public void enterTheUsername(String uName) {
	    driver.findElementById("username").sendKeys(uName);
	}

	@And("Enter the Password as (.*)")
	public void enterThePassword(String pwd) {
	    driver.findElementById("password").sendKeys(pwd);
	}

	@And("Click submit button")
	public void clickSubmitButton() {
	    driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Click CRM\\/SFA")
	public void clickCRMSFA() {
	   driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
	}

	@And("Click Leads")
	public void clickLeads() {
	   driver.findElementByXPath("//a[text()='Leads']").click();
	}

	@And("Click Create Lead")
	public void clickCreateLead() {
	    driver.findElementByXPath("//a[text()='Create Lead']").click();
	}

	@And("Enter Company Name as (.*)")
	public void enterCompanyName(String cName) {
	   driver.findElementById("createLeadForm_companyName").sendKeys(cName);
	}

	@And("Enter First Name as (.*)")
	public void enterFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);  
	}

	@And("Enter Last Name as (.*)")
	public void enterLastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@And("Click Create Lead Submit button")
	public void clickCreateLeadSubmitButton() {
	    driver.findElementByXPath("//input[@class='smallSubmit']").click();
	}

	@When("Verify Company Name as (.*)")
	public void verifyCompanyName(String ComName) {
	   String text = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText();
	   if (text.contains(ComName)) {
		   System.out.println("Create Lead is success");
	   }
	   else {
		   System.out.println("Create Lead is not success");
	   }
	}

	@Then("Verify Create Lead is success")
	public void verifyCreateLeadIsSuccess() {
	    System.out.println("Create Lead is completed");
	    driver.close();
	}



}
*/