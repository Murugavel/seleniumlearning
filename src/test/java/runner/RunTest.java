package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\java\\feature\\DuplicateLead.feature", 
					glue = { "com.yalla.pages","Steps" }, 
					monochrome = true)
//dryRun = true, snippets = SnippetType.CAMELCASE, tegs ="@smoke and @reg" tags="~@smoke" )
public class RunTest {

}
