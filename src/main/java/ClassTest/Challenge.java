package ClassTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Challenge {

	@Test
	public static void vendor() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("murugavel.mail@gmail.com");
		driver.findElementById("password").clear();
		driver.findElementById("password").sendKeys("Hunter123$");
		driver.findElementById("buttonLogin").click();
		Thread.sleep(3000);
		Actions action = new Actions(driver);
		WebElement vendor = driver.findElementByXPath("//button[text()=' Vendors']");
		action.moveToElement(vendor).build().perform();
		driver.findElementByLinkText("Search for Vendor").click();
		//driver.findElementByXPath("//a[@href='/vendors/search']").click();
		String id = "DE987564";
		driver.findElementById("vendorTaxID").sendKeys(id);
		driver.findElementById("buttonSearch").click();
		//String text = driver.findElementByXPath("//table[@class='table']/tbody/tr[2]/td[1]").getText();
		String text = driver.findElementByXPath("//td[text()='"+id+"']/preceding::td").getText();
		System.out.println("Vendor name is : "+text);
		driver.close(); 
		

	}

}
