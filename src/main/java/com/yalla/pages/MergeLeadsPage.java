package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MergeLeadsPage extends Annotations{ 

	public MergeLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="//img[@src='/images/fieldlookup.gif']") WebElement eleFromLead;
	@And("Click From Lead")
	public FindLeadsForMerge clickFromLead() {
		clickWithNoSnap(eleFromLead);  
		switchToWindow(1);
		return new FindLeadsForMerge();

	}
	
	@FindBy(how=How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement eleToLead;
	@And("Click To Lead")
	public FindLeadsForMerge clickToLead() {
		clickWithNoSnap(eleToLead);  
		switchToWindow(1);
		return new FindLeadsForMerge();

	}
	
	@FindBy(how=How.XPATH, using="//a[text()='Merge']") WebElement eleMergeLeadButton;
	@And("Click Merge Lead Button")
	public ViewLeadPage clickMergeLeadButton() {
		clickWithNoSnap(eleMergeLeadButton);  
		acceptAlert();
		return new ViewLeadPage();

	}


	
	

	


}







