package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class FindLeadsForMerge extends Annotations{ 

	public FindLeadsForMerge() {
       PageFactory.initElements(driver, this);
	} 

		
	@FindBy(how= How.XPATH, using="//input[contains(@class, 'x-form-text x-form-field')]") WebElement eleLeadIDForMerge;
	@And("Enter Lead ID For Merge as (.*)")
	public FindLeadsForMerge enterLeadIDForMerge(String data) {
		clearAndType(eleLeadIDForMerge, data);  
		return this;
	}
	
	@FindBy(how= How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeadButton;
	@And("Click Find Lead Button For Merge")
	public FindLeadsForMerge clickFindLeadButtonForMerge() throws InterruptedException {
		click(eleFindLeadButton);  
		Thread.sleep(2000);
		return this;
	}
	
	@FindBy(how= How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleFirstLeadID;
	@And("Click First Lead ID For Merge")
	public  MergeLeadsPage clickFirstLeadIDForMerge() {
		clickWithNoSnap(eleFirstLeadID);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
	
	





}







