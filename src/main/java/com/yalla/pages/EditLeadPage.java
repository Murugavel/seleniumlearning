package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EditLeadPage extends Annotations{ 

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	} 

		
	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleCompanyNameChange;
	@And("Change Company Name as (.*)")
	public EditLeadPage changeCompanyName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleCompanyNameChange, data);  
		return this;

	}
	
	@FindBy(how=How.NAME, using="submitButton") WebElement eleUpdateBUtton;
	@When("Click Update button")
	public ViewLeadPage clickUpdateButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleUpdateBUtton);  
		return new ViewLeadPage();

	}
	
	@Then("Edit Lead is Success")
	public void editLeadSUccess() {
		System.out.println("Edit Lead is Success");
	}

	

	


}







