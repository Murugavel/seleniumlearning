package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class FindLeadsPage extends Annotations{ 

	public FindLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

		
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@And("Enter First Name in Find Lead as (.*)")
	public FindLeadsPage enterFirstNameInFindLead(String data) {
		clearAndType(eleFirstName, data);  
		return this;
	}
	
	@FindBy(how=How.NAME, using="id") WebElement eleLeadID;
	@And("Enter Lead ID in Find Lead as (.*)")
	public FindLeadsPage enterLeadIDInFindLead(String data) {
		clearAndType(eleLeadID, data);  
		return this;
	}

	@FindBy(how=How.XPATH, using="(//input[@name='companyName'])[2]") WebElement eleCompanyName;
	@And("Enter Company Name in Find Lead as (.*)")
	public FindLeadsPage enterCompanyNameInFindLead(String data) {
		clearAndType(eleCompanyName, data);  
		return this;
	}

	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeadsButton;
	@And("Click Find Leads Button")
	public FindLeadsPage clickFindLeadsButton() throws InterruptedException {
		click(eleFindLeadsButton);  
		Thread.sleep(5000);
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleFirstLeadID;
	@And("Click First Leads ID")
	public ViewLeadPage clickFirstLeadID() {
		click(eleFirstLeadID);  
		return new ViewLeadPage();
	}
	
	@FindBy(how=How.CLASS_NAME, using="x-paging-info") WebElement eleLeadIDVerification;
	@Then("Verify Lead ID in Find Leads")
	public FindLeadsPage verifyLeadIDInFindLead() {
		//Thread.sleep(5000);
		System.out.println("Search Result is: "+eleLeadIDVerification.getText());
		verifyPartialText(eleLeadIDVerification, "No records to display");
		//verifyExactText(eleLeadIDVerification, "No records to display");
		//Thread.sleep(5000);
		return this;
	}
	

	


}







