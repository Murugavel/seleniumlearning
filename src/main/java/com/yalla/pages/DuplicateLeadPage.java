package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.When;

public class DuplicateLeadPage extends Annotations{ 

	public DuplicateLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.NAME, using="submitButton") WebElement eleSubmit;
	@When("Click Duplicate Submit")
	public ViewLeadPage clickDuplicateSubmit() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleSubmit);  
		return new ViewLeadPage();

	}
	
	

}







