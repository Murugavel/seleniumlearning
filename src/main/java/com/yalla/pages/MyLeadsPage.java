package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPage extends Annotations{ 

	public MyLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="//a[text()='Create Lead']") WebElement eleCreateLead;
	@And("Click Create Lead")
	public CreateLeadPage clickCreateLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLead);  
		return new CreateLeadPage();

	}
	
	@FindBy(how=How.XPATH, using="//a[text()='Find Leads']") WebElement eleFindLeads;
	@And("Click Find Lead")
	public FindLeadsPage clickFindLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLeads);  
		return new FindLeadsPage();

	}
	
	@FindBy(how=How.XPATH, using="//a[text()='Merge Leads']") WebElement eleMergeLeads;
	@And("Click Merge Lead")
	public MergeLeadsPage clickMergeLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleMergeLeads);  
		return new MergeLeadsPage();

	}


	


}







