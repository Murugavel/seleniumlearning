package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@And("Enter Company Name as (.*)")
	public CreateLeadPage enterCompanyName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleCompanyName, data);
		//click(eleCompanyName);  
		return this;
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@And("Enter First Name as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleFirstName); 
		clearAndType(eleFirstName, data);
		return this;
	}
	
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@And("Enter Last Name as (.*)")
	public CreateLeadPage enterLastName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLastName); 
		clearAndType(eleLastName, data);
		return this;  
	}
	
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit']") WebElement eleCreateLeadButton;
	@When("Click Create Lead Submit button")
	public ViewLeadPage clickCreateLeadButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLeadButton);  
		return new ViewLeadPage();
	}
	
	
	
	


}







