package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement eleVerifyFirstName;
	@Then("Verify First Name as (.*)")
	public ViewLeadPage verifyFirstName(String data) {
		verifyExactText(eleVerifyFirstName, data);
		return this;

	}
	
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleEditButton;
	@And("Click Edit Button")
	public EditLeadPage clickEditButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleEditButton);  
		//Thread.sleep(5000);
		return new EditLeadPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Delete") WebElement eleDeleteButton;
	@And("Click Delete Button")
	public MyLeadsPage clickDeleteButton() throws InterruptedException {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleDeleteButton);  
		Thread.sleep(5000);
		return new MyLeadsPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement eleFindLeadsFromView;
	@And("Click Find Leads From View")
	public FindLeadsPage clickFindLeadsFromView() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLeadsFromView);  
		//Thread.sleep(5000);
		return new FindLeadsPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement eleDuplicateLeadButton;
	@And("Click Duplicate Lead Button")
	public DuplicateLeadPage clickDuplicateLeadButton() throws InterruptedException {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleDuplicateLeadButton);  
		Thread.sleep(5000);
		return new DuplicateLeadPage();
	}
	
	
	
 

}







