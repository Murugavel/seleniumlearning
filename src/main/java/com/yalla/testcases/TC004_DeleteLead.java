package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DeleteLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DeleteLead";
		testcaseDec = "Deleting the Lead";
		author = "Murugavel";
		category = "smoke";
		excelFileName = "TC_DeleteLead";
	} 

	@Test(dataProvider="fetchData") 
	public void deleteLead(String uName, String pwd, String LeadID, String verifyLeadID) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterLeadIDInFindLead(LeadID) 
		.clickFindLeadsButton()
		.clickFirstLeadID()
		.clickDeleteButton()
		.clickFindLeads()
		.enterLeadIDInFindLead(verifyLeadID)
		.clickFindLeadsButton()
		.verifyLeadIDInFindLead();
		
	}
	
}






