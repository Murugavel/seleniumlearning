package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Editing the Lead";
		author = "Murugavel";
		category = "smoke";
		excelFileName = "TC_EditLead";
	} 

	@Test(dataProvider="fetchData") 
	public void editLead(String uName, String pwd, String fName, String cName) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterFirstNameInFindLead(fName)
		.clickFindLeadsButton()
		.clickFirstLeadID()
		.clickEditButton()
		.changeCompanyName(cName)
		.clickUpdateButton();
		
	}
	
}






