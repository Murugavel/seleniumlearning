package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC006_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC006_DuplicateLead";
		testcaseDec = "Duplicate the Lead";
		author = "Murugavel";
		category = "smoke";
		excelFileName = "TC_DuplicateLead";
	} 

	@Test(dataProvider="fetchData") 
	public void deleteLead(String uName, String pwd, String cName, String fName) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enterCompanyNameInFindLead(cName)
		.clickFindLeadsButton()
		.clickFirstLeadID()
		.clickDuplicateLeadButton()
		.clickDuplicateSubmit()
		.verifyFirstName(fName);
		
	}
	
}






