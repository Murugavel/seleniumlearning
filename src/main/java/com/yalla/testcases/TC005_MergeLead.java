package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC005_MergeLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC005_MergeLead";
		testcaseDec = "Merging the Lead";
		author = "Murugavel";
		category = "smoke";
		excelFileName = "TC_MergeLead";
	} 

	@Test(dataProvider="fetchData") 
	public void editLead(String uName, String pwd, String fLead, String tLead) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLeads()
		.clickFromLead()
		.enterLeadIDForMerge(fLead)
		.clickFindLeadButtonForMerge()
		.clickFirstLeadIDForMerge()
		.clickToLead()
		.enterLeadIDForMerge(tLead)
		.clickFindLeadButtonForMerge()
		.clickFirstLeadIDForMerge()
		.clickMergeLeadButton()
		.clickFindLeadsFromView()
		.enterLeadIDInFindLead(fLead)
		.clickFindLeadsButton()
		.verifyLeadIDInFindLead();
	}
	
}






